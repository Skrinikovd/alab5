package danieil.com.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activity3 extends AppCompatActivity {

    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
    }

    public void launch4(View view){
        Intent i = new Intent(this,Activity4.class);
        startActivityForResult(i,0);
    }

    public void launch5(View view){
        Intent i = new Intent(this,Activity5.class);
        startActivityForResult(i,0);
    }

    protected void onActivityResult(int request, int result, Intent i){
        setResult(RESULT_OK,i);
    }
}
