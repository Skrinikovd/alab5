package danieil.com.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    public void launch1(View view){
        Intent i = new Intent();
        i.putExtra("extra","Activity 2: One, 1, uno, un/une" );
        setResult(RESULT_OK,i);
        finish();
    }

    public void launch2(View view){
        Intent i = new Intent();
        i.putExtra("extra","Activity 2: Two, 2 , due, deux" );
        setResult(RESULT_OK,i);
        finish();
    }
}
