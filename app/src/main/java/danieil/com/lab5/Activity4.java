package danieil.com.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Activity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);
    }

    public void returnValue(View view){
        EditText e = (EditText)findViewById(R.id.inputTxt);
        if(e.getText().toString().length() > 0){
            Intent i = new Intent();
            i.putExtra("extra",e.getText().toString());
            setResult(RESULT_OK,i);
        }
        finish();
    }
}
