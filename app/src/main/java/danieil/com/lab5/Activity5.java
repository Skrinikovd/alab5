package danieil.com.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activity5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
    }

    public void callFinish(View view){
        finish();
    }

    @Override
    public void finish(){
        Intent i = new Intent();
        i.putExtra("extra","Dave. I'm afraid I can't do that. " );
        setResult(RESULT_OK,i);
        super.finish();
    }
}
