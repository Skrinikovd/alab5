package danieil.com.lab5;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launch2(View view){
        Intent i = new Intent(this,Activity2.class);
        startActivityForResult(i,0);
    }

    public void launch3(View view){
        Intent i = new Intent(this,Activity3.class);
        startActivityForResult(i,0);
    }

    protected void onActivityResult(int request, int result, Intent i){
        String data;

        if(result == RESULT_OK){
            if(i!=null &&i.hasExtra("extra")) {
                data = i.getExtras().getString("extra");

                if(data != null)
                    ((TextView)findViewById(R.id.tv)).append(data+"\n");
            }
        }
    }

    //Taken right from stackoverflow
    public void return1(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        startActivity(browserIntent);
    }

    public void return2(View view){
        Uri u = Uri.parse("geo:0,0");

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, u);

        if (mapIntent.resolveActivity(getPackageManager()) != null)
            startActivity(mapIntent);
    }
}
